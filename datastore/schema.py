# -*- coding: utf-8 -*-

from sqlalchemy import (Column, Unicode, Table, SmallInteger, Integer, ARRAY, String, ForeignKey,
                        create_engine, MetaData)

from common import config


class MoviesDataSchema(object):

    def __init__(self, metaData):
        """
        Provides database schema.
        """

        self.titles = Table('titles', metaData,
                            Column('t_const', String,  primary_key=True, autoincrement=False, nullable=False),
                            Column('title_type', Unicode(100)),
                            Column('primary_title', Unicode()),
                            Column('original_title', Unicode()),
                            Column('is_adult', SmallInteger),
                            Column('start_year', SmallInteger),
                            Column('end_year', SmallInteger),
                            Column('runtime_minutes', Integer),
                            Column('genres', ARRAY(String)),
                            )

        self.people = Table('people', metaData,
                            Column('n_const', String, primary_key=True, autoincrement=False, nullable=False),
                            Column('primary_name', Unicode(250)),
                            Column('birth_year', SmallInteger),
                            Column('death_year', SmallInteger),
                            Column('primary_profession', ARRAY(String)),
                            )

        self.people_to_titles = Table('people_to_titles', metaData,
                                      Column('t_const', String, ForeignKey(self.titles.c.t_const), nullable=False),
                                      Column('n_const', String, ForeignKey(self.people.c.n_const), nullable=False),
                                      )


connection_string = config['database']['connection_string']
db_engine = create_engine(connection_string, pool_size=20)
meta_data = MetaData(db_engine)
movies_data_schema = MoviesDataSchema(meta_data)