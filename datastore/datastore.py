# -*- coding: utf-8 -*-

from contextlib import closing
from sqlalchemy import select, bindparam

from datastore.schema import db_engine, movies_data_schema


class DataStore(object):

    def __init__(self):
        self._session = db_engine.connect()

    def _load(self, query, params=None):
        """
        Loads data from database.
        """
        with closing(self._session.execute(query, params)) as rs:
            result = [dict(row) for row in rs]
            return result

    def _loadPaged(self, query, start, count, params=None):
        """
        Version of _load method that supports paging
        """

        query = query.limit(count).offset(start)
        return self._load(query, params)


class MoviesDataStore(DataStore):
    """
    A data store to handle database operations on movies.
    """

    def __init__(self):
        super(MoviesDataStore, self).__init__()
        self.titles = TitlesDataStore()
        self.people = PeopleDataStore()

    def create_database_schema(self):

        movies_data_schema.titles.create()
        movies_data_schema.people.create()
        movies_data_schema.people_to_titles.create()

    def add_people_to_title(self, t_const, n_const):

        self._session.execute(movies_data_schema.people_to_titles.insert(),
                              dict(t_const=t_const,
                                   n_const=n_const)
                              )


class TitlesDataStore(DataStore):
    """
    A data store to handle database operations on movie titles.
    """

    def create(self, t_const, title_type, primary_title, original_title, is_adult, start_year, end_year,
               runtime_minutes, genres):

        self._session.execute(movies_data_schema.titles.insert(),
                              dict(t_const=t_const,
                                   title_type=title_type,
                                   primary_title=primary_title,
                                   original_title=original_title,
                                   is_adult=is_adult,
                                   start_year=start_year,
                                   end_year=end_year,
                                   runtime_minutes=runtime_minutes,
                                   genres=genres)
                              )

    def get_by_start_year(self, start_year, start, count):
        query = select(
            columns=[movies_data_schema.titles.c.t_const,
                     movies_data_schema.titles.c.title_type,
                     movies_data_schema.titles.c.primary_title,
                     movies_data_schema.titles.c.original_title,
                     movies_data_schema.titles.c.is_adult,
                     movies_data_schema.titles.c.start_year,
                     movies_data_schema.titles.c.end_year,
                     movies_data_schema.titles.c.runtime_minutes,
                     movies_data_schema.titles.c.genres,
                     ],
            from_obj=movies_data_schema.titles,
            whereclause=movies_data_schema.titles.c.start_year == bindparam('star_year'),
            order_by=movies_data_schema.titles.c.primary_title
        )

        return self._loadPaged(query, start, count, dict(star_year=start_year))

    def get_by_genre(self, genre,  start, count):
        query = select(
            columns=[movies_data_schema.titles.c.t_const,
                     movies_data_schema.titles.c.title_type,
                     movies_data_schema.titles.c.primary_title,
                     movies_data_schema.titles.c.original_title,
                     movies_data_schema.titles.c.is_adult,
                     movies_data_schema.titles.c.start_year,
                     movies_data_schema.titles.c.end_year,
                     movies_data_schema.titles.c.runtime_minutes,
                     movies_data_schema.titles.c.genres,
                     ],
            from_obj=movies_data_schema.titles,
            whereclause=movies_data_schema.titles.c.genres.any(bindparam('genre')),
            order_by=movies_data_schema.titles.c.primary_title
        )

        return self._loadPaged(query, start, count, dict(genre=genre))

    def get_by_person_ids(self, ids):
        query = select(
            columns=[movies_data_schema.people_to_titles.c.n_const,
                     movies_data_schema.titles.c.t_const,
                     movies_data_schema.titles.c.title_type,
                     movies_data_schema.titles.c.primary_title,
                     movies_data_schema.titles.c.original_title,
                     movies_data_schema.titles.c.is_adult,
                     movies_data_schema.titles.c.start_year,
                     movies_data_schema.titles.c.end_year,
                     movies_data_schema.titles.c.runtime_minutes,
                     movies_data_schema.titles.c.genres,
                     ],
            from_obj=movies_data_schema.people_to_titles.join(
                movies_data_schema.titles,
                movies_data_schema.people_to_titles.c.t_const == movies_data_schema.titles.c.t_const
            ),
            whereclause=movies_data_schema.people_to_titles.c.n_const.in_(ids),
        )

        return self._load(query, dict(ids=ids))


class PeopleDataStore(DataStore):
    """
    A data store to handle database operations on movie people.
    """

    def create(self, n_const, primary_name, birth_year, death_year, primary_profession):
        self._session.execute(movies_data_schema.people.insert(),
                              dict(n_const=n_const,
                                   primary_name=primary_name,
                                   birth_year=birth_year,
                                   death_year=death_year,
                                   primary_profession=primary_profession
                                   )
                              )

    def get_by_title_ids(self, title_ids):

        query = select(
            columns=[movies_data_schema.people_to_titles.c.t_const,
                     movies_data_schema.people.c.n_const,
                     movies_data_schema.people.c.primary_name,
                     movies_data_schema.people.c.birth_year,
                     movies_data_schema.people.c.death_year,
                     movies_data_schema.people.c.primary_profession,
                     ],
            from_obj=movies_data_schema.people_to_titles.join(movies_data_schema.people,
                   movies_data_schema.people_to_titles.c.n_const == movies_data_schema.people.c.n_const),
            whereclause=movies_data_schema.people_to_titles.c.t_const.in_(title_ids)
        )

        return self._load(query, dict(title_ids=title_ids))

    def get_with_name(self, name):
        query = select(
            columns=[movies_data_schema.people.c.n_const,
                     movies_data_schema.people.c.primary_name,
                     movies_data_schema.people.c.birth_year,
                     movies_data_schema.people.c.death_year,
                     movies_data_schema.people.c.primary_profession,
                     ],
            whereclause=movies_data_schema.people.c.primary_name.match(bindparam('name'))
        )

        return self._load(query, dict(name=name))

