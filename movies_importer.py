# -*- coding: utf-8 -*-
import csv
import gzip
from datetime import datetime
from collections import namedtuple
from datastore.datastore import MoviesDataStore


TitleRow = namedtuple('title_row', ['t_const', 'title_type', 'primary_title', 'original_title', 'is_adult',
                                    'start_year', 'end_year', 'runtime_minutes', 'genres', ])

NameRow = namedtuple('name_row', ['n_const', 'primary_name', 'birth_year', 'death_year', 'primary_profession',
                                  'known_for_titles'])

_movies_data_store = MoviesDataStore()


def do_import():

    file_path = 'title.basics.tsv.gz'
    print("Importing {0}".format(file_path))
    _do_import(file_path, add_title, TitleRow)

    file_path = 'name.basics.tsv.gz'
    print("Importing {0}".format(file_path))

    _do_import(file_path, add_person, NameRow)
    print("Done")


def _do_import(file_path, callback, row_type):
    """
    I will import data from specified files
    :param file_path: gzip file
    :param callback: A function that will be called when processing subsequent lines
    :param row_type: type of row e.g. TitleRow
    """

    with gzip.open(file_path, mode="rt", encoding='UTF-8') as f:
        reader = csv.reader(f, delimiter="\t", quotechar='\x07')
        next(reader)
        for row in reader:
            row = [None if item == '\\N' else item for item in row]
            row = row_type(*row)
            callback(row)

def add_title(title):
    """
    Helper for add title to database
    :param title: movie title
    :rtype title: TitleRow
    """
    genres = title.genres.split(',') if title.genres else None
    _movies_data_store.titles.create(title.t_const, title.title_type, title.primary_title,
                                     title.original_title, title.is_adult, title.start_year,
                                     title.end_year, title.runtime_minutes, genres)


def add_person(person):
    """
    Helper for add person to database
    :param person: person
    :rtype person: NameRow
    """
    primary_profession = person.primary_profession.split(',') if person.primary_profession else None
    _movies_data_store.people.create(person.n_const, person.primary_name, person.birth_year,
                                     person.death_year, primary_profession)

    if person.known_for_titles:
        for t_const in person.known_for_titles.split(','):
            _movies_data_store.add_people_to_title(t_const, person.n_const)
