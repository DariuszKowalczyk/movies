from werkzeug import script
from datastore.datastore import MoviesDataStore
from movies_importer import do_import
from rest_api.api import app


def action_create_database_schema():
    """I will create movies database schema"""
    ds = MoviesDataStore()
    ds.create_database_schema()
    print("Movies database schema was created")


def action_import():
    """I will import movies to specific database"""
    do_import()


def action_rest_api():
    """Starts rest api server"""
    app.run()

if __name__ == '__main__':
    script.run()

