# -*- coding: utf-8 -*-
from collections import defaultdict

from datastore.datastore import MoviesDataStore


class Movies(object):

    def __init__(self):
        self._movies_data_store = MoviesDataStore()

    def get_movies_by_start_year_paged(self, start_year, page, page_size):
        """
        Returns movies with specified start year
        :param start_year:
        :param page: page number of search results
        :param page_size:number of results per page
        """

        start = page * page_size
        titles = self._movies_data_store.titles.get_by_start_year(start_year, start, page_size)

        title_to_people = self._get_people_by_title_ids(titles)
        for title in titles:
            title['people'] = title_to_people.get(title['t_const'], [])

        return titles

    def _get_people_by_title_ids(self, titles):
        """
        Returns people for specified titles
        :param titles: lis of titles
        :return: dict (t_const: dict())
        """
        title_ids = [title['t_const'] for title in titles]
        people = self._movies_data_store.people.get_by_title_ids(title_ids)
        title_to_people = defaultdict(list)
        for person in people:
            title_to_people[person['t_const']].append(person)

        return title_to_people

    def get_movies_by_genre_paged(self, genre, page, page_size):
        """
        Returns movies with specified genre
        :param genre: movie genre
        :param page: page number of search results
        :param page_size: number of results per page
        :return: list of dict
        """

        start = page * page_size
        titles = self._movies_data_store.titles.get_by_genre(genre, start, page_size)
        title_to_people = self._get_people_by_title_ids(titles)

        for title in titles:
            title['people'] = title_to_people.get(title['t_const'], [])

        return titles

    def get_with_person_name(self, name):
        """
        Returns the people with the given name and movies that the actors belongs to.
        :param name: actor name
        :return: list of dict()
        """
        people = self._movies_data_store.people.get_with_name(name)

        people_ids = [person['n_const'] for person in people]
        titles = self._movies_data_store.titles.get_by_person_ids(people_ids)

        people_to_titles = defaultdict(list)
        for title in titles:
            people_to_titles[title['n_const']].append(title)

        for person in people:
            person['titles'] = people_to_titles.get(person['n_const'], [])

        return people





