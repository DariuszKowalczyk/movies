Simple library for import and sharing by Rest API movies and people from title.basics.tsv.gz and name.basics.tsv.gz files

##Usage:
For create database schema: python start.py create_database_schema

For run import: python start.py import

For run Rest Api server: python start.py create_database_schema

##Examples of api requests:

```
http://localhost:5000/movies/?with_person_name=Bergman
http://localhost:5000/movies/?start_year=1894&page=0&page_size=50
http://localhost:5000/movies/?start_year=1894&page=1
http://localhost:5000/movies/?genre=Documentary&page=0&page_size=50
http://localhost:5000/movies/?genre=Documentary&page=0
```
When we do not specify the page_size parameter, then default is 100 results per page
