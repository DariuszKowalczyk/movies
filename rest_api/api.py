from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
from core import Movies

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('start_year')
parser.add_argument('page')
parser.add_argument('page_size')
parser.add_argument('genre')
parser.add_argument('with_person_name')


class MoviesResource(Resource):

    _movies = Movies()

    def get(self):

        args = parser.parse_args()
        start_year = args.get('start_year')
        page = args.get('page')
        page_size = args.get('page_size') or 100
        genre = args.get('genre')
        actor_name = args.get('with_person_name')

        try:
            if start_year and page:
                # e.g. http://localhost:5000/movies/?genre=Documentary&page=0&page_size=100
                return self._movies.get_movies_by_start_year_paged(start_year, int(page), int(page_size))
            if genre and page:
                # e.g. http://localhost:5000/movies/?start_year=1894&page=1&page_size=100
                return self._movies.get_movies_by_genre_paged(genre, int(page), int(page_size))
            if actor_name:
                # e. g. http://localhost:5000/movies/?with_actor_name=Bergman
                return self._movies.get_with_person_name(actor_name)
        except ValueError:
            pass

        return abort(400, message="The request does not have parameters")

api.add_resource(MoviesResource, '/movies/')
